# react-native-card-reader-demo

reader card

## Installation

```sh
npm install react-native-card-reader-demo
```

## Usage

```js
import { multiply } from "react-native-card-reader-demo";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
