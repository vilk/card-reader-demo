package com.reactnativecardreaderdemo;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

@ReactModule(name = CardReaderDemoModule.NAME)
public class CardReaderDemoModule extends ReactContextBaseJavaModule {
    public static final String NAME = "CardReaderDemo";
    public CardReaderManagement cardReader;

    public CardReaderDemoModule(ReactApplicationContext reactContext) {
        super(reactContext);
        cardReader= new CardReaderManagement(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }


    // Example method
    // See https://reactnative.dev/docs/native-modules-android
    @ReactMethod
    public void multiply(int a, int b, Promise promise) {
        promise.resolve(a * b);
    }

    @ReactMethod
    public void startScanCard(Promise promise) {
      cardReader.startScanCard(promise);
    }

    @ReactMethod
    public void stopScanCard() {
      cardReader.stopScanCard();
    }

    public static native int nativeMultiply(int a, int b);
}
