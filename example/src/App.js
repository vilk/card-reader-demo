import * as React from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  NativeModules,
} from 'react-native';
import { multiply } from 'react-native-card-reader-demo';
const { startScanCard, stopScanCard } = NativeModules.CardReaderDemo;

export default function App() {
  const [result, setResult] = React.useState();

  React.useEffect(() => {
    multiply(3, 7).then(setResult);
  }, []);

  const handleScan = () => {
    startScanCard().then((map) => console.log(map));
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleScan}>
        <Text>Scan ha</Text>
      </TouchableOpacity>
      <Text>Result: {result}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
